<?php

/**
 * Данний клас оброблює запит від клієнта, валідує і відравляє E-mail.
 *
 * @arguments: $_POST.
 * Кожен запит має знаходитись в ассоціативному массиві:
 *     - з ключом form, для звичайних данних.
 *     - з ключом file, для файлів.
 * Вся вложенність массива буде ініціалізована в данному функціоналі.
 *
 * @return:
 * Status: 200, 1     - Повідомлення відправлено!
 * Status: 200, NULL  - Повідомлення не було надіслано!
 * Status: 500, Error - Помилка відправки!
 *
 * Class SendEmail
 * @namespace \
 * @author vlad.golubtsov@ukr.net
 */
class SendEmail
{
    /**
     * @var array: Данні з форми.
     */
    private $request;

    /**
     * @var string: Техт для E-mail.
     */
    private $textMessage = '';

    /**
     * @var string: Отримувач E-mail.
     */
    private $recipientMessage;

    /**
     * @var string: Тема E-mail.
     */
    private $subjectMessage;

    /**
     * @var string: Заголовок запиту E-mail.
     */
    private $headerMessage = '';

    /**
     * Констуктор классу викликається при ініціалізації класу.
     * Тут ми переопріділяємо всі данні.
     *
     * SendEmail constructor.
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Відправка E-mail.
     */
    public function send()
    {
        return mail(
            $this->recipientMessage,
            $this->subjectMessage,
            $this->textMessage,
            $this->headerMessage
        );
    }

    /**
     * Валідуємо запит. Забираємо зайвий HTML, ХSS та SQL-інєкції.
     * Кожен елемент массива, який прийшов з POST-запиту, має ідентичні
     * властивості. Кожен ключ з JS співпадає з ключом в РНР.
     *     - Якщо запит прийшов у вигляді JSON - перебудовую його в массив,
     *       а потім в об'єкт.
     *     - Якщо запит прийшов у вигляді массиву - перебудовую його зразу
     *       в об'єкт.
     *
     * @return $this
     */
    public function validateRequest()
    {
        if (!$request = json_decode($this->request['form'], true)) {
            $request = $this->request;
        }

        foreach ($request as $key => $value) {
            $value = htmlspecialchars($value);
            $value = strip_tags($value);
            $value = stripslashes($value);
            $value = trim($value);

            if (!is_object($this->request)) {
                $this->request = new stdClass();
            }

            $this->request->{$key} = $value;
        }

        return $this;
    }

    /**
     * Використовуючи дизайн паттерн @Factory, будуємо заголовок запиту E-mail.
     * Заголовок запиту повідомлення: $this->headerMessage
     * Загловок повідомлення:         $this->subjectMessage
     * Отримувач повідомлення:        $this->recipientMessage
     * Текст повідомлення:            $this->textMessage
     *
     * @return $this
     */
    public function getAction()
    {
        switch ($this->request->action) {
            case 'action_1':
                $this->headerMessage .= "From: Vlad <vlad.golubtsov@ukr.net>\r\n";
                $this->headerMessage .= "Reply-To: vlad@ukr.net\r\n";

                $this->subjectMessage = 'Subject_1';
                $this->recipientMessage = 'vlad.golubtsov@ukr.net';

                $this->textMessage .= "Name: " . $this->request->name;
                $this->textMessage .= "Age: " . $this->request->age;
                $this->textMessage .= "Gender: " . $this->request->gender;
                break;

            case 'action_2':
                $textMessage = '';
                $file = 'test.php';
                $content = file_get_contents($file);
                $content = chunk_split(base64_encode($content));
                $fileName = basename($file);
                $uniqueId = md5(uniqid(time()));

                $this->headerMessage .= "From: Vlad <vlad.golubtsov@ukr.net>\r\n";
                $this->headerMessage .= "Reply-To: vlad@ukr.net\r\n";
                $this->headerMessage .= "MIME-Version: 1.0\r\n";
                $this->headerMessage .= "Content-Type: multipart/mixed; boundary=\"{$uniqueId}\"\r\n\r\n";

                $this->subjectMessage = 'Subject_1';
                $this->recipientMessage = 'vlad.golubtsov@ukr.net';

                $textMessage .= "Name: " . $this->request->name;
                $textMessage .= "Age: " . $this->request->age;
                $textMessage .= "Gender: " . $this->request->gender;

                $this->textMessage .= "--{$uniqueId}\r\n";
                $this->textMessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
                $this->textMessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
                $this->textMessage .= $textMessage . "\r\n\r\n";
                $this->textMessage .= "--{$uniqueId}\r\n";
                $this->textMessage .= "Content-Type: application/octet-stream; name=\"{$fileName}\"\r\n";
                $this->textMessage .= "Content-Transfer-Encoding: base64\r\n";
                $this->textMessage .= "Content-Disposition: attachment; filename=\"{$fileName}\"\r\n\r\n";
                $this->textMessage .= $content . "\r\n\r\n";
                $this->textMessage .= "--{$uniqueId}--";
                break;
        }

        return $this;
    }
}

/**
 * Якщо прийшов POST-запит, викликаєм класс SendEmail.
 *     - Валідуємо запит.
 *     - Отримуємо дію, згідно активної дії, за допомогою дизайн паттерна @Factory.
 *     - Відправляю E-Mail.
 *
 * @return:
 * Status: 200, 1     - Повідомлення відправлено!
 * Status: 200, NULL  - Повідомлення не було надіслано!
 * Status: 500, Error - Помилка відправки!
 */
if ($_POST) {
    $sendEmail = new SendEmail($_POST);
    echo $sendEmail
        ->validateRequest()
        ->getAction()
        ->send();
}